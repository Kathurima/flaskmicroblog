��    9      �              �     �     �     �     �      �             <   /     l     ~  /   �     �     �     �     �     �     �               +     0     M     Z     `  	   g     q     }     �  #   �  %   �      �     �          
          &     =     L     `     n     v     ~  A   �     �     �     �     �               (  #   H     l     �     �     �  %   �  �       �	     �	     �	     �	     �	     
     
  D   !
  $   f
  #   �
  0   �
     �
     �
     �
               #     *     I     _  *   f     �     �     �     �     �     �     �  .   �  0   $  -   U     �     �  
   �     �     �     �     �     �                 D        c     y     �  .   �     �  %   �  (   �  #   !  "   E  +   h      �  !   �  #   �   %(count)d followers %(count)d following %(username)s wrote %(when)s About me An unexpected error has occurred Back Changes saved! Check your email for the instructions to reset your password Click to Reset It Click to Sign Up! Congratulations, you are now a registered user! Edit Profile Edit your profile Email Explore File Not Found Follow Forgot Your Password? Hi, %(username)s! Home Invalid username or password Last seen on Login Logout New User? Newer posts Older posts Password Please sign in to access this page. Please use a different email address. Please use a different username. Post Profile Remember Me Repeat Password Request Password Reset Reset Password Reset Your Password Say something Sign In Sign Up Submit The administrator has been notified. Sorry for the inconvenience! Toggle navigation Unfollow User User %(username)s not found. Username Welcome to Oreon Microblog You are following %(username)s! You are not following %(username)s. You cannot follow yourself! You cannot unfollow yourself! Your password has been reset. Your post is now live! [Oreon Microblog] Reset Your Password Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2020-06-02 13:48+0300
PO-Revision-Date: 2020-06-02 13:49+0300
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 %(count)d seguidores siguiendo a %(count)d %(username)s enscribio %(when)s Acerca de mí Ha ocurrido un error inesperado Atrás Cambios salvados! Busca en tu email las instrucciones para crear una nueva contraseña Haz click aquí para pedir una nueva ¡Haz click aquí para Registrarte! ¡Felicitaciones, ya eres un usuario registrado! Editar Perfil Editar tu perfil Emai Explorar Página No Encontrada Seguir ¿Te olvidaste tu contraseña? ¡Hola, %(username)s! Inicio Nombre de usuario o contraseña inválidos Última visita Ingresar Salir ¿Usuario Nuevo? Artículos siguientes Artículos previos Contraseña Por favor ingrese para acceder a esta página. Por favor use una dirección de email diferente. Por favor use un nombre de usuario diferente. Enviar Perfil Recordarme Repetir Contraseña Pedir una nueva contraseña Restablecer la Contraseña Nueva Contraseña Dí algo Ingresar Regístrate Enviar El administrador ha sido notificado. ¡Lamentamos la inconveniencia! Navegacion de palanca Dejar de seguir Usuario El usuario %(username)s no ha sido encontrado. Nombre de usuario msgstr "Bienvenido a Oreon Microblog" ¡Ahora estás siguiendo a %(username)s! No estás siguiendo a %(username)s. ¡No te puedes seguir a tí mismo! ¡No te puedes dejar de seguir a tí mismo! Tu contraseña ha sido cambiada. ¡Tu artículo ha sido publicado! [Oreon Microblog] Nueva Contraseña 