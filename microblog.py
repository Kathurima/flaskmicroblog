# This script defines the Flask application instance

from app import create_app, db, cli
from app.models import User, Post

app = create_app()
cli.register(app)


# Creating a shell context that adds the database instance and models
# to the shell session
@app.shell_context_processor    # This function registers the function as a shell context function
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Post}
